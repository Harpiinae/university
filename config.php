<?php
session_start();

$config = array();
$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

$config['db'] = array();

/* Web server settings */
$config['db']['host'] = 'localhost';
$config['db']['user'] = 'root';
$config['db']['pass'] = '';
$config['db']['dbname'] = 'university';

$settings['settings'] = $config;