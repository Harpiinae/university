<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require __DIR__ . '/vendor/autoload.php';

require __DIR__ .'/config.php';

$app = new \Slim\App($settings);
$container = $app->getContainer();

require __DIR__  . '/containers.php';

$app->any('/firstpage', function (Request $request, Response $response, array $args) {
    $persons = Array();

    if($_POST) {
        $stmt = $this->db->prepare("
SELECT
	PERSON.Surname,
	PERSON.Name,
	PERSON.Patronymic

FROM PERSON
JOIN STUDENT
	ON STUDENT.Student_ID = PERSON.Student_ID
JOIN STUDENT_GROUP
	ON STUDENT_GROUP.Student_ID = STUDENT.Student_ID
JOIN GROUPS
	ON GROUPS.Group_ID = STUDENT_GROUP.Group_ID
JOIN SPECIALITY
	ON SPECIALITY.Speciality_ID = GROUPS.Speciality_ID
JOIN CAFEDRA
	ON CAFEDRA.Cafedra_ID = SPECIALITY.Cafedra_ID
WHERE
	CAFEDRA.Cafedra_name LIKE ? AND 
	SPECIALITY.Speciality_name LIKE ? AND 
	GROUPS.Group_CODE LIKE ?
	");
        $cafedra =  '%' . $_POST['cafedra']  . '%';
        $speciality =  '%' . $_POST['speciality']  . '%';
        $group =  '%' . $_POST['group']  . '%';
        $stmt->bindParam(1, $cafedra);
        $stmt->bindParam(2, $speciality);
        $stmt->bindParam(3, $group);
        $stmt->execute();
        $persons = $stmt->fetchAll();
    }

    $body = $this->view->fetch('firstpage.phtml', ['persons' => $persons,'cafedra' => $_POST['cafedra'],'speciality' => $_POST['speciality'],'group' => $_POST['group']]);
    $this->view->render($response, 'partials/master.phtml', ['body' => $body]);
    return $response;
});

$app->any('/secondpage', function (Request $request, Response $response, array $args) {
    $persons = Array();

    if($_POST) {
        $stmt = $this->db->prepare("
SELECT
	SUBJECT.Subject_name,
    SMARK.Mark_name
    
FROM PERSON
JOIN STUDENT
	ON STUDENT.Student_ID = PERSON.Student_ID
JOIN TEACH_PLAN
	ON TEACH_PLAN.Tester_ID = PERSON.Person_ID
JOIN SUBJECT
	ON SUBJECT.Subject_ID = TEACH_PLAN.Subject_ID
JOIN STUDENT_MARKS
	ON STUDENT_MARKS.Teach_plan_ID = TEACH_PLAN.Teach_plan_ID AND STUDENT_MARKS.Student_ID = STUDENT.Student_ID
JOIN SMARK
	ON SMARK.Mark_ID = STUDENT_MARKS.Mark_ID
WHERE PERSON.Surname LIKE ? AND 
    PERSON.Name LIKE ? AND 
	PERSON.Patronymic LIKE ?
	");
        $lastName =  '%' . $_POST['lastName']  . '%';
        $firstName =  '%' . $_POST['firstName']  . '%';
        $patronymic =  '%' . $_POST['patronymic']  . '%';
        $stmt->bindParam(1, $lastName);
        $stmt->bindParam(2, $firstName);
        $stmt->bindParam(3, $patronymic);
        $stmt->execute();
        $persons = $stmt->fetchAll();
    }

    $body = $this->view->fetch('secondpage.phtml', ['persons' => $persons,'firstName' => $_POST['firstName'],'lastName' => $_POST['lastName'],'patronymic' => $_POST['patronymic']]);
    $this->view->render($response, 'partials/master.phtml', ['body' => $body]);
    return $response;
});

$app->any('/thirdpage', function (Request $request, Response $response, array $args) {
    $persons = Array();

    if($_POST) {
        $stmt = $this->db->prepare("
SELECT
	SUBJECT.Subject_name,
    SUBJECT.Subject_shifr
    
FROM SUBJECT
JOIN TEACH_PLAN
	ON TEACH_PLAN.Subject_ID = SUBJECT.Subject_ID
JOIN SEMESTER
	ON SEMESTER.Semester_ID = TEACH_PLAN.Semester_ID
JOIN GROUPS
	ON GROUPS.Group_ID = TEACH_PLAN.Group_ID
    
WHERE GROUPS.Group_code LIKE ?
GROUP BY SUBJECT.Subject_name, SUBJECT.Subject_shifr
	");
        $group =  '%' . $_POST['group']  . '%';
        $stmt->bindParam(1, $group);
        $stmt->execute();
        $persons = $stmt->fetchAll();
    }

    $body = $this->view->fetch('thirdpage.phtml', ['persons' => $persons,'group' => $_POST['group']]);
    $this->view->render($response, 'partials/master.phtml', ['body' => $body]);
    return $response;
});

$app->any('/fourthpage', function (Request $request, Response $response, array $args) {
    $persons = Array();

    if($_POST) {
        $stmt = $this->db->prepare("
SELECT
	PAYMENT.Payment_date,
    PAYMENT.Payment_sum,
    PAYMENT.Document_no
    
FROM PERSON
JOIN STUDENT
	ON STUDENT.Student_ID = PERSON.Student_ID
JOIN SFINANCE
	ON SFINANCE.Finance_ID = STUDENT.Finance_ID
JOIN CONTRACT
	ON CONTRACT.Student_ID = STUDENT.Student_ID
JOIN SCONTRACT_KIND
	ON SCONTRACT_KIND.Contract_kind_ID = CONTRACT.Contract_kind_ID
JOIN PAYMENT
	ON PAYMENT.Contract_ID = CONTRACT.Contract_ID
WHERE PERSON.Surname LIKE ? AND 
    PERSON.Name LIKE ? AND 
	PERSON.Patronymic LIKE ?
	");
        $lastName =  '%' . $_POST['lastName']  . '%';
        $firstName =  '%' . $_POST['firstName']  . '%';
        $patronymic =  '%' . $_POST['patronymic']  . '%';
        $stmt->bindParam(1, $lastName);
        $stmt->bindParam(2, $firstName);
        $stmt->bindParam(3, $patronymic);
        $stmt->execute();
        $persons = $stmt->fetchAll();
    }

    $body = $this->view->fetch('fourthpage.phtml', ['persons' => $persons,'firstName' => $_POST['firstName'],'lastName' => $_POST['lastName'],'patronymic' => $_POST['patronymic']]);
    $this->view->render($response, 'partials/master.phtml', ['body' => $body]);
    return $response;
});

$app->run();